#!/bin/sh
#****************************************************************#
# ScriptName: finetune.sh
# Author: $SHTERM_REAL_USER@alibaba-inc.com
# Create Date: 2022-08-09 19:40
# Modify Author: $SHTERM_REAL_USER@alibaba-inc.com
# Modify Date: 2022-08-09 19:40
# Function: 
#***************************************************************#
python tools/run_net.py \
	--cfg configs/Kinetics/TimeSformer_divST_8x32_224_4gpus.yaml \
	DATA.PATH_TO_DATA_DIR /root/dataset/k400/ \
	NUM_GPUS 4 \
	TRAIN.BATCH_SIZE 8 \
