#!/bin/sh
#****************************************************************#
# ScriptName: infer.sh
# Author: $SHTERM_REAL_USER@alibaba-inc.com
# Create Date: 2022-08-08 11:55
# Modify Author: $SHTERM_REAL_USER@alibaba-inc.com
# Modify Date: 2022-08-16 11:09
# Function: 
#***************************************************************#
python tools/run_net.py \
	--cfg configs/Kinetics/TimeSformer_divST_8x32_224_TEST.yaml \
	DATA.PATH_TO_DATA_DIR /root/dataset/k400/ \
	TEST.CHECKPOINT_FILE_PATH pretrain/TimeSformer_divST_8x32_224_K400.pyth \
	TRAIN.ENABLE False \
