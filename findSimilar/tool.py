import math
import os
from typing import final

from PIL import Image
import numpy as np

from scipy.spatial.distance import pdist
from scipy.spatial.distance import squareform
from torch.nn.modules.activation import Threshold
import torch

class Stack(object):

    def __init__(self):
        self.stack = []
        self.top = -1

    def push(self, data):
        """
        进栈函数
        """
        self.stack.append(data)
        self.top += 1

    def isEmpty(self):
        """"
        判断栈是否为空
        """
        if(self.top==-1):
            return True
        else:
            return False
    
    def pop(self):
        """
        出栈函数，
        """
        if(self.isEmpty()):
            print('stack is empty!')
            return None
        else:
            self.top -= 1
            return self.stack.pop()

    def gettop(self):
        """
        取栈顶
        """
        if(self.isEmpty()):
            print('stack is empty!')
            return None
        else:
            return self.stack[self.top]


def drawImage(imagePath, soft):
    head_num = soft.size[0]
    
    # print softmax gray image
    for k in range(head_num):
        if not os.path.isdir(imagePath):
            os.makedirs(imagePath)
        imageArray = soft[0][k]
        imageArray = imageArray.detach().numpy()
        shape = imageArray.shape
        tempImage = np.ones(shape)
        imageArray *= 255
        tempImage *= 255
        imageArray = tempImage - imageArray
        im = Image.fromarray(imageArray)
        im = im.convert('L')  # 这样才能转为灰度图，如果是彩色图则改L为‘RGB’
        im.save(imagePath+'/head_'+str(k)+'.png')


def headDistance(head1, head2):
    
    head_shape = head1.shape
    disSum = 0
    
    for i in range(head_shape[0]):
        for j in range(head_shape[1]):
            dis = head1[i][j] - head2[i][j]
            # disSum += dis if dis>=0 else -dis
            disSum += dis * dis
    
    return disSum


def allHeadDistance(soft):
    
    head_num = soft.shape[0]
    distance = np.zeros((head_num, head_num), dtype='float32')
    #是否已经算了
    computed = np.zeros((head_num, head_num))
    
    for i in range(head_num):
        for j in range(head_num):
            if i==j:
                computed[i][j] = 1

            if not (computed[i][j] and computed[j][i]):
                dis = headDistance(soft[i], soft[j])
                distance[i][j] = distance[j][i] = dis
                computed[i][j] = computed[j][i] = 1
                    
    return distance


def findSimilarHead(task, soft):
    '''
    soft.shape = [batch, head, token, token]
    '''

    threshold_task = {
        "mrpc": 0.00075,#old
        'qnli': 18,#new
        'qqp': 18,#new
        'sst2': 6,#new
        'stsb': 5,#new
        'rte': 6,#old
        'mnli': 18,#new
        'cola': 0.00075,#old
        'squad': 15
    }

    threshold = threshold_task[task]# * soft.shape[2] * soft.shape[3]

    batch_size = soft.shape[0]

    head_distance_batch = []
    head_distance_sum = 0

    for i in range(batch_size):
        # compute distance between different heads
        head_distance = allHeadDistance(soft[i])
        head_distance_batch.append(head_distance)
        head_distance_sum += head_distance

    # print(head_distance)

    isSimilar_batch = (np.array(head_distance_batch) <= threshold)

    isSimilar = isSimilar_batch[0]

    for i in range(1, batch_size):
        isSimilar = isSimilar * isSimilar_batch[i]

    head_num = head_distance.shape[0]
    #最后相似的链表
    similarHead = []
    #是否已经找到组织了
    # isadd = [False for i in range(head_num)]

    for i in range(head_num):
        similar = [i]

        for j in range(head_num):
            if(i!=j and isSimilar[i][j]):
                similar.append(j)
        
        similarHead.append(similar)
    
    return threshold, head_distance_sum, similarHead


def createTree(distance):
    head_num = distance.shape[0]
    INF = 40000
    
    #统计每个节点连着的节点数
    distance_threshold, similar_head = findSimilarHead(distance)
    link_head_num = []
    for i in range(head_num):
        link_head_num.append(len(similar_head[i])-1)
    
    #到自己的dis调成max值
    for i in range(head_num):
        distance[i][i] = INF

    #根节点，-1表示根节点，i自己表示无根叶节点，!=i表示有根叶节点
    root = [i for i in range(head_num)]
    #自己是否可以继续连其他根（节点）
    ableAddToOtherRoot = [True for i in range(head_num)]
    #每个节点连接的节点
    link_head = [[] for i in range(head_num)]

    dis_min = distance.min()
    dis_min_index = distance.argmin()
    dis_min_index_x = dis_min_index / head_num
    dis_min_index_y = dis_min_index % head_num
    
    while(dis_min<=distance_threshold):
        head_begin = dis_min_index_x if dis_min_index_y>dis_min_index_x else dis_min_index_y
        head_end = dis_min_index_y if dis_min_index_y>dis_min_index_x else dis_min_index_x

        # if(root[head_begin]!=head_begin or root[head_end]!=head_end):
        #     distance[head_begin][head_end] = INF
        #     distance[head_end][head_begin] = INF
        #     continue

        #先确定谁是根，连的边数多的是根，如果相同则比较dis和，再相同默认序号小的
        if(link_head_num[head_begin]>link_head_num[head_end]):
            head_root = head_begin
            head_leaf = head_end
        elif(link_head_num[head_end]>link_head_num[head_begin]):
            head_root = head_end
            head_leaf = head_begin
        else:
            sum_begin = 0
            sum_end =0
            #统计dis和
            for i in range(link_head_num[head_begin]):
                sum_begin += distance[sum_begin][similar_head[i+1]]
                sum_end += distance[sum_end][similar_head[i+1]]
            #比较dis和
            if(sum_begin>sum_end):
                head_root = head_end
                head_leaf = head_begin
            else:
                head_root = head_begin
                head_leaf = head_end

        #验证root是否还可以继续接收其他节点，如果root节点是无根叶节点or根节点，就可以继续加（自己最多只连一层），否则就不可以接收了（自己本来就连在别人身上）
        #验证leaf是否无组织
        if((root[head_root]==head_root or root[head_root]==-1) and root[head_leaf]==head_leaf):
            #将leaf加到root的树中
            root[head_leaf] = head_root
            link_head[head_root].append(head_leaf)
            #leaf节点已经找到组织了
            ableAddToOtherRoot[head_leaf] = False
            #将distance矩阵对应距离置为INF
            distance[head_root][head_leaf] = INF
            distance[head_leaf][head_root] = INF
            #leaf的边都置为INF，因为已经找到组织

        dis_min = distance.min()
        dis_min_index = distance.argmin()
        dis_min_index_x = dis_min_index / head_num
        dis_min_index_y = dis_min_index % head_num


def treeNumAndDis(forest_set, distance):#root_set是生成的所有森林
    #森林数
    forest_num = len(forest_set)
    head_num = distance.shape[0]

    #每个森林树的棵树和森林总距离
    tree_num = [0 for i in range(forest_num)]
    dis = [0 for i in range(forest_num)]

    for forest_id, forest in enumerate(forest_set):#每个森林
        for i in range(head_num):
            if(forest[i]==-1):#根
                tree_num[forest_id] += 1
            else:#叶子
                dis[forest_id] += distance[forest[i]][i]
    
    return tree_num, dis


def processRemainNode(head_num, similar_head, final_root, root, has_home_node_num):
    #copy一份root关系，以便后续返回不修改原关系
    root_copy = []
    for i in range(head_num):
        root_copy.append(root[i])

    #递归结束条件
    #所有节点都有组织了
    if(has_home_node_num==head_num):
        #保存当前建成的树
        final_root.append(root_copy)
        print('root_copy = {}'.format(root_copy))
        return
    
    #递归主体
    for i in range(head_num):
        #i已经已经有组织了（or是根）
        if(root_copy[i]!=i):
            continue
        
        #i置为根
        root_copy[i] = -1
        has_home_node_num += 1

        #处理入栈根i连的叶
        for j in range(1, len(similar_head[i])):
            leaf = similar_head[i][j]
            #如果leaf还没组织
            if(root_copy[leaf]==leaf):
                root_copy[leaf] = i 
                has_home_node_num += 1
        
        #处理剩余节点
        processRemainNode(head_num, similar_head, final_root, root_copy, has_home_node_num)

        #复原i为根之前的状态
        #根i连的叶复原
        for j in range(1, len(similar_head[i])):
            leaf = similar_head[i][j]
            if(root[leaf]==leaf):
                root_copy[leaf] = root[leaf] 
                has_home_node_num -= 1

        #根i置为叶
        root_copy[i] = root[i]
        has_home_node_num -= 1
        

def processWholeGraph(head_num, similar_head, final_root, root, has_home_num):
    #保存处理的中间状态
    #森林栈
    forestStack = Stack()
    #被处理的节点数
    hasHomeNodeNumStack = Stack()

    #防止改变原始root
    root_copy = []
    for i in range(head_num):
        root_copy.append(root[i])  
    
    #初始节点压栈
    forestStack.push(root_copy)
    hasHomeNodeNumStack.push(has_home_num)

    while(not forestStack.isEmpty()):
        forest = forestStack.pop()
        has_home_node_num = hasHomeNodeNumStack.pop()

        if(has_home_node_num<12):
            for i in range(head_num):
                #未被处理的节点依次处理后分别压栈
                if(forest[i]==i):
                    root_copy = []
                    has_home_node_num_copy = has_home_node_num
                    for j in range(head_num):
                        root_copy.append(forest[j])
                    
                    #处理
                    #i置为根
                    root_copy[i] = -1
                    has_home_node_num_copy += 1
                    #处理入栈根i连的叶
                    for j in range(1, len(similar_head[i])):
                        leaf = similar_head[i][j]
                        #如果leaf还没组织
                        if(root_copy[leaf]==leaf):
                            root_copy[leaf] = i 
                            has_home_node_num_copy += 1
                    
                    #处理完压栈
                    forestStack.push(root_copy)
                    hasHomeNodeNumStack.push(has_home_node_num_copy)
        else:
            #得到结果，添加到final_rot
            final_root.append(forest)
            print(forest)  


def roughCreateForest(task, soft):
    '''
    soft.shape = [batch, head, token, token]
    '''
    head_num = soft.shape[1]
    #不同head之间dis矩阵
    #每个节点连着的节点
    distance_threshold, head_distance, similar_head = findSimilarHead(task, soft)

    #用来保存所有建成的树
    final_root = []
    #找到组织的节点数
    has_home_node_num = 0
    #初始化图
    root = [i for i in range(head_num)]
    
    #递归暴力建树
    # processRemainNode(head_num, similar_head, final_root, root, has_home_node_num)
    #非递归暴力建树
    processWholeGraph(head_num, similar_head, final_root, root, has_home_node_num)

    print('final_root len = '+str(len(final_root)))
    
    final_root_set = [list(t) for t in set(tuple(_) for _ in final_root)]
    final_root_set.sort(key=final_root.index)# 用于输出排序结果
    
    print('final_root_set len = '+str(len(final_root_set)))

    #计算生成所有森林的内部情况
    tree_num, dis = treeNumAndDis(final_root_set, head_distance)

    return distance_threshold, final_root_set, tree_num, dis


def findSubSet(batch_soft):
    batch_size = batch_soft.shape[0]
    head_num = batch_soft.shape[1]
    #[batch_size, head_num]
    batch_similar_head = []
    #最终子集结果
    batch_similar_head_subset = []

    for i in range(batch_size):
        head_distance = allHeadDistance(batch_soft[i])
        # print(head_distance)
        distance_threshold, head_distance, similar_head = findSimilarHead(batch_soft[i])
        print('softmax_threshold = {}'.format(distance_threshold))
        print('head_distance = \n{}'.format(head_distance))
        print('similar_head = {}'.format(similar_head))
        batch_similar_head.append(similar_head)
    
    for j in range(head_num):
        similar = set([])
        for i in range(batch_size):
            similar = similar.intersection(set(batch_similar_head[i][j]))
        
        batch_similar_head_subset.append(list(similar))
    
    return batch_similar_head_subset
    

def takeSecond(elem):
        return elem[1]


def printTree(task, layer, soft): 
    '''
    soft.shape = [batch, head, token, token]
    '''
    #生成树
    threshold, forest_root, tree_num, dis = roughCreateForest(task, soft)
    
    #排序
    forest = [[] for i in range(len(forest_root))]
    for i in range(len(forest_root)):
        forest[i].append(tree_num[i])
        forest[i].append(dis[i])
        forest[i].append(forest_root[i])
    
    # 指定第二个元素dis排序
    forest.sort(key=takeSecond)
    #再第一个元素tree_num排序
    forest.sort()

    with open('forest_'+task+'.txt', 'a') as f:
        if(layer==0):
            f.write('theshold='+str(threshold)+'\n\n')
            print('theshold='+str(threshold)+'\n')
        f.write('layer='+str(layer)+'\n')
        print('layer='+str(layer))
        f.write('[tree_num, dis, forest_root]'+'\n')
        for forestId, line in enumerate(forest):
            print('forest_'+str(forestId)+':'+str(line))
            f.write('forest_'+str(forestId)+':'+str(line)+'\n')
        f.write('\n')
        print('\n')


def findSimilarLayerInput(embedding_token, layer):
    # [batch_size, 1+(224/16)^2=197, hidden_size=768]
    
    threshold = [700 for i in range(12)]

    batch_size = embedding_token.shape[0]
    token_num = embedding_token.shape[1]
    hidden_size = embedding_token.shape[2]

    # [batch_size*197, hidden_size=768]
    embedding = np.array(embedding_token, copy=True)
    embedding = embedding.reshape(-1, hidden_size)

    #L1距离
    dist_original = pdist(embedding, metric='minkowski', p=1)
    #稀疏表示转为对称矩阵
    dist_original = squareform(dist_original)
    print('input_dist_original = \n{}'.format(dist_original))

    #不同batch的token间是否可连
    similar = np.zeros(dist_original.shape)
    
    for row in range(dist_original.shape[0]):
        #row对应第几个batch
        row_batchId = int(row/token_num)
        for line in range(row+1, dist_original.shape[1]):
            #line对应第几个batch
            # line_batchId = int(line/token_num)
            if(dist_original[row][line]<=threshold[layer]):
                if(not (row in range(row_batchId*token_num, (row_batchId+1)*token_num) and line in range(row_batchId*token_num, (row_batchId+1)*token_num))):
                    #不位于三角形区域
                    similar[row][line] = similar[line][row] = 1
    
    #cls_token不可替换
    for row_batchId in range(batch_size):
        similar[row_batchId*token_num] = np.zeros((1, dist_original.shape[1]))[0]
    for line_batchId in range(batch_size):
        similar[:, line_batchId*token_num] = np.zeros((dist_original.shape[0], 1))[:, 0]
        

    return similar

    
def replaceLayerInput(embedding_token, layer):
    # print('没改时:')
    # print(embedding_token)
    embedding = embedding_token.cpu().detach().numpy()
    batch_size = embedding_token.shape[0]
    token_num = embedding_token.shape[1]
    hidden_size = embedding_token.shape[2]

    all_token_num = batch_size * token_num
    
    similar = findSimilarLayerInput(embedding, layer)    
    # print('similar = {}'.format(similar))

    #有组织的token
    has_home = np.array([0 for i in range(all_token_num)])
    #有组织的token数
    has_home_num = np.sum(has_home)
    #记录自己的home,初始化默认自己
    root = np.array([i for i in range(all_token_num)])

    while(has_home_num<all_token_num):
        similar_num = np.sum(similar, axis=1) 
        link_max = similar_num.argmax()
        
        # 1个连的都没有为止
        if(similar_num[link_max]>=1):
            #记录替换
            root[link_max] = -1
            has_home[link_max] = 1
            #对于link_max当前已经处理的与其相连的num
            link_num = 0
            for j in range(all_token_num):
                #提前结束遍历
                if(link_num==similar_num[link_max]):
                    break

                if(similar[link_max][j]==1):
                    root[j] = link_max
                    has_home[j] = 1
                    link_num += 1
                    #更新similar信息
                    for k in range(all_token_num):
                        similar[j][k] = similar[k][j] = 0
        else:#剩下没有连的了
            break
        
        has_home_num = np.sum(has_home)

    #按照root做替换
    # #先简单处理一下token与batch的对应关系
    # root_all_batch = []
    # for i in range(batch_size):
    #     root_batch = []
    #     for j in range(token_num):
    #         if(root[i*batch_size+j*token_num]==-1):
    #             root_batch.append([[i, j], True])
    #         else:
    #             root_batch.append([[root[i*batch_size+j*token_num]/token_num, root[i*batch_size+j*token_num]%token_num], False])
        
    #     root_all_batch.append(root_batch)
    #替换的token数
    replace_token_num =0
    not_myself_replace_num = 0
    # for i in range(all_token_num):
    #     if(root[i]==i or root[i]==-1):
    #         continue
    #     #前一半的
    #     if(i<token_num and root[i]!=token_num+i):
    #         not_myself_replace_num += 1
    #     #后一半的
    #     if(i>=token_num and root[i]!=i-token_num):
    #         not_myself_replace_num += 1

    # print('root = {}\nnot_myself_replace_num = {}'.format(root, not_myself_replace_num))
    print('root = {}'.format(root))
    for i in range(all_token_num):
        if(root[i]!=i and root[i]!=-1):
            # print('绝对相似')
            # different = embedding_token[int(i/token_num)][i%token_num]-embedding_token[int(root[i]/token_num)][root[i]%token_num]
            # dis = 0
            # # for j in range(hidden_size):
            # #     if(different[j]<0):
            # #         different[j]=-different[j]
            # dis = torch.dot(different, different)
            # if(dis==0):
            #     print('自己')
            # else:
            # #     print('我算的dis = {}'.format(dis))
            # #     print('库算的dis = {}'.format(dist_original[root[i]][i]))
            #     with open('./different.txt', 'a') as f:
            #         f.write('替换'+str(i)+', root['+str(i)+']='+str(root[i])+'\n')
            #         f.write('embedding_token[i]='+str(embedding_token[int(i/token_num)][i%token_num].cpu().detach().numpy())+'\n')
            #         f.write('embedding_token[root]='+str(embedding_token[int(root[i]/token_num)][root[i]%token_num].cpu().detach().numpy())+'\n')
            #         f.write('\n')


            # print('different = \n{}'.format(embedding_token[int(i/token_num)][i%token_num]-embedding_token[int(root[i]/token_num)][root[i]%token_num]))
            # print('embedding_token[(root[i]/token_num][rot[i]\%token_num] = \n{}'.format(embedding_token[int(root[i]/token_num)][root[i]%token_num]))
            embedding_token[int(i/token_num)][i%token_num] = embedding_token[int(root[i]/token_num)][root[i]%token_num]
            replace_token_num += 1
    # print('replace rate = {}%'.format(100*replace_num/all_token_num))
    # print('改完:')
    # print(embedding_token)        
    return embedding_token, replace_token_num, not_myself_replace_num