#coding:utf-8
from math import dist
from tool import findSimilarHead, printTree
import numpy as np
import os
import time

import argparse

# format = '%-*s%-*s\n'

# for layer in range(12):
#     # [batch, head, token, token]
#     attention = np.load('softmax_mprc_layer'+str(layer)+'.npy')
#     threshold, head_distance, similar_head = findSimilarHead(attention[0])
#     with open('similarHead.txt', 'a') as f:
#         f.write('theshold='+str(threshold)+'\n')
#         f.write('layer='+str(layer)+'\n')
#         print('layer='+str(layer))
#         for headId, line in enumerate(similar_head):
#             print(str(line))
#             f.write('head_'+str(headId)+':'+str(line)+'\n')
#         f.write('\n')

def parse_args():
    parser = argparse.ArgumentParser(description="Finetune a transformers model on a text classification task")
    parser.add_argument(
        "--task",
        type=str,
        help="The name of the task.",
        required=True,
    )
    args = parser.parse_args()
    return args

args = parse_args()

# [layer, batch, head, token, token]
attention = np.load('softmax_results.npy')
# [batch, layer, head, token, token]
# attention = np.transpose(attention, (1, 0, 2, 3, 4))
# print(attention.shape)

for layer in range(attention.shape[0]):
    threshold, head_distance, similar_head = findSimilarHead(args.task, attention[layer])
    with open('similarHead_'+args.task+'.txt', 'a') as f:
        print('theshold='+str(threshold))
        if(layer==0):
            f.write('theshold='+str(threshold)+'\n')
        f.write('layer='+str(layer)+'\n')
        print('layer='+str(layer))
        for headId, line in enumerate(similar_head):
            head_str = 'head_'+str(headId)+':'+str(line)
            f.write(head_str.ljust(35))
            dis = []
            for link_node in line:
                dis.append(head_distance[headId][link_node])
            dis_str = 'dis:'+str(dis)
            f.write(dis_str.ljust(50))
            f.write('\n')
            # f.write(format % (35, head_str, 50, dis_str))
            print('{headList:<{headListLen}}{disList:<{disListLen}}'.format(headList='head_'+str(headId)+':'+str(line), headListLen=35, disList='dis:'+str(dis), disListLen=50))
        f.write('\n')
        print('\n')

# starttime = time.time()

for layer in range(attention.shape[0]):
    printTree(args.task, layer, attention[layer])