# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved.
# Copyright 2020 Ross Wightman
# Modified Model definition

import torch
import torch.nn as nn
from functools import partial
import math
import warnings
import torch.nn.functional as F
import numpy as np

from timesformer.models.vit_utils import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD
from timesformer.models.helpers import load_pretrained
from timesformer.models.vit_utils import DropPath, to_2tuple, trunc_normal_

from .build import MODEL_REGISTRY
from torch import einsum
from einops import rearrange, reduce, repeat

_layer_mark = 0
global_layer = -1

soft_result_time = torch.tensor([]).to('cuda')
soft_result_space = torch.tensor([]).to('cuda')

total_token_num_time = 0
repeat_token_num_time = 0
total_token_num_space = 0
repeat_token_num_space = 0


def _cfg(url='', **kwargs):
    return {
        'url': url,
        'num_classes': 1000, 'input_size': (3, 224, 224), 'pool_size': None,
        'crop_pct': .9, 'interpolation': 'bicubic',
        'mean': IMAGENET_DEFAULT_MEAN, 'std': IMAGENET_DEFAULT_STD,
        'first_conv': 'patch_embed.proj', 'classifier': 'head',
        **kwargs
    }


default_cfgs = {
    'vit_base_patch16_224': _cfg(
        url='https://github.com/rwightman/pytorch-image-models/releases/download/v0.1-vitjx/jx_vit_base_p16_224-80ecf9dd.pth',
        mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5),
    ),
}

class Mlp(nn.Module):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Linear(hidden_features, out_features)
        self.drop = nn.Dropout(drop)

    def forward(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x

class Attention(nn.Module):
    def __init__(self, dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0., with_qkv=True, soft_type='time'):
        super().__init__()
        self.num_heads = num_heads
        head_dim = dim // num_heads
        self.scale = qk_scale or head_dim ** -0.5
        self.with_qkv = with_qkv
        if self.with_qkv:
           self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
           self.proj = nn.Linear(dim, dim)
           self.proj_drop = nn.Dropout(proj_drop)
        self.attn_drop = nn.Dropout(attn_drop)
        self.soft_type = soft_type

    def forward(self, x):
        B, N, C = x.shape
        #----------------------------------------------------------------------------------------------------------------------------#
        #global _layer_mark
        #if(_layer_mark==0):
        #    print('q_before = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        if self.with_qkv:
           qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
           q, k, v = qkv[0], qkv[1], qkv[2]
        else:
           qkv = x.reshape(B, N, self.num_heads, C // self.num_heads).permute(0, 2, 1, 3)
           q, k, v  = qkv, qkv, qkv

        #----------------------------------------------------------------------------------------------------------------------------#
        #if(_layer_mark==0):
        #    print('q = ',q.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        attn = (q @ k.transpose(-2, -1)) * self.scale
        #----------------------------------------------------------------------------------------------------------------------------#
        #if(_layer_mark==0):
        #    print('q_kT = ',attn.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        attn = attn.softmax(dim=-1)
        attn = self.attn_drop(attn)
        
        attn_temp = attn.clone().detach()

        global global_layer
        if(self.soft_type=='time'):
            if global_layer == 0:
                for _i in [1, 2, 3, 6, 7, 8, 9, 10, 11]: attn_temp[:,_i] = attn_temp[:,5]

            if global_layer == 1:
                for _i in [0, 1, 2, 3, 4, 5, 7, 8, 9, 10, 11]: attn_temp[:,_i] = attn_temp[:,6]

            if global_layer == 2:
                for _i in [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11]: attn_temp[:,_i] = attn_temp[:,9]

            if global_layer == 3:
                for _i in [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11]: attn_temp[:,_i] = attn_temp[:,9]

            if global_layer == 4:
                for _i in [0, 1, 3, 4, 5, 6, 7, 8, 9, 10]: attn_temp[:,_i] = attn_temp[:,11]

            if global_layer == 5:
                for _i in [0, 1, 2, 3, 5, 7, 8, 9, 10]: attn_temp[:,_i] = attn_temp[:,11]

            if global_layer == 6:
                for _i in [4, 7, 8]: attn_temp[:,_i] = attn_temp[:,6]
                for _i in [3, 10]: attn_temp[:,_i] = attn_temp[:,11]
                for _i in [9]: attn_temp[:,_i] = attn_temp[:,2]

            if global_layer == 7:
                for _i in [0, 5, 9]: attn_temp[:,_i] = attn_temp[:,11]
                for _i in [8]: attn_temp[:,_i] = attn_temp[:,3]
                for _i in [1]: attn_temp[:,_i] = attn_temp[:,6]
            
            #if global_layer == 8:
            #    for _i in [7, 8, 9]: attn_temp[:,_i] = attn_temp[:,5]
            #    for _i in [4, 6]: attn_temp[:,_i] = attn_temp[:,0]
            
            if global_layer == 9:
                for _i in [0]: attn_temp[:,_i] = attn_temp[:,9]
            
            #if global_layer == 10:
            #    for _i in [0, 1, 3, 5, 6, 7, 10]: attn_temp[:,_i] = attn_temp[:,11]
            #    for _i in [4]: attn_temp[:,_i] = attn_temp[:,2]
            
            if global_layer == 11:
                for _i in [11]: attn_temp[:,_i] = attn_temp[:,0]
                for _i in [5]: attn_temp[:,_i] = attn_temp[:,1]
                
        elif(self.soft_type=='space0'):
            #if global_layer == 0:
            #    for _i in [7, 9]: attn[:,_i] = attn[:,3]
            
            if global_layer == 1:
                for _i in [0, 6]: attn_temp[:,_i] = attn_temp[:,10]
            
            #if global_layer == 2:
            #    for _i in [1, 7, 11]: attn_temp[:,_i] = attn_temp[:,5]
            
            #if global_layer == 3:
            #    for _i in [1, 2, 3, 4, 5, 7, 8, 10, 11]: attn[:,_i] = attn[:,6]

            if global_layer == 4:
                for _i in [4, 5, 8, 10]: attn_temp[:,_i] = attn_temp[:,7]

            if global_layer == 5:
                for _i in [0, 1, 3, 7, 9]: attn_temp[:,_i] = attn_temp[:,2]

            if global_layer == 6:
                for _i in [6, 10, 11]: attn_temp[:,_i] = attn_temp[:,7]

            #if global_layer == 7:
            #    for _i in [0, 1, 6, 7, 8, 9]: attn[:,_i] = attn[:,3]
            #    for _i in [5]: attn[:,_i] = attn[:,4]

            #if global_layer == 8:
            #    for _i in [5, 9, 10]: attn[:,_i] = attn[:,8]

            #if global_layer == 9:
            #    for _i in [4, 5]: attn[:,_i] = attn[:,9]
            #    for _i in [1, 2, 3, 6, 7]: attn[:,_i] = attn[:,10]

            if global_layer == 10:
                for _i in [1, 5, 11]: attn_temp[:,_i] = attn_temp[:,6]

            #if global_layer == 11:
            #    for _i in [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]: attn[:,_i] = attn[:,1]
        
        else:
            pass
        
        attn = attn_temp.clone()

        x = (attn @ v).transpose(1, 2).reshape(B, N, C)
        #----------------------------------------------------------------------------------------------------------------------------#
        #if(_layer_mark==0):
        #    print('s_v = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        if self.with_qkv:
           x = self.proj(x)
           x = self.proj_drop(x)
        return x

class Block(nn.Module):

    def __init__(self, dim, num_heads, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0.1, act_layer=nn.GELU, norm_layer=nn.LayerNorm, attention_type='divided_space_time'):
        super().__init__()
        self.attention_type = attention_type
        assert(attention_type in ['divided_space_time', 'space_only','joint_space_time'])

        self.norm1 = norm_layer(dim)
        self.attn = Attention(
           dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop, soft_type='space')

        ## Temporal Attention Parameters
        if self.attention_type == 'divided_space_time':
            self.temporal_norm1 = norm_layer(dim)
            self.temporal_attn = Attention(
              dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop, soft_type='time')
            self.temporal_fc = nn.Linear(dim, dim)

        ## drop path
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)


    def forward(self, x, B, T, W):
        num_spatial_tokens = (x.size(1) - 1) // T
        H = num_spatial_tokens // W

        if self.attention_type in ['space_only', 'joint_space_time']:
            x = x + self.drop_path(self.attn(self.norm1(x)))
            x = x + self.drop_path(self.mlp(self.norm2(x)))
            return x
        elif self.attention_type == 'divided_space_time':
            #------------------------------------------------------------------------------------------------------------------------#
            #global _layer_mark
            #if(_layer_mark==0):
            #    print('attn_before = ',x.shape)
            #------------------------------------------------------------------------------------------------------------------------#
            ## Temporal
            xt = x[:,1:,:]
            #--------------------------------------------------------------------------------------------------------------------------------#
            #更关注原始数据，在normal之前做也合理，但可能阈值要变大
            global total_token_num_time
            global repeat_token_num_time
            global total_token_num_space
            global repeat_token_num_space
            total_token_num_time += B * T * H * W
            total_token_num_space += B * T * H * W

            input_thres = 15.0
            #直接frame*token_num展平，全求距离
            dist_map = torch.cdist(xt, xt, p=2).to('cuda')
            dist_mask = dist_map<input_thres #b (h w t) (h w t)
            
            ##对角线为0，自己和自己不连
            #dig_mask = torch.eye(dist_mask.shape[1], dtype=torch.bool).to('cuda')
            #dist_mask *= (~dig_mask)
            
            #只能T帧内连，且自己不能连
            mask_T = torch.zeros(dist_mask.shape, dtype=torch.bool).to('cuda')
            for patch_id in range(H*W):
                mask_T[:, patch_id*T:(patch_id+1)*T, patch_id*T:(patch_id+1)*T] = True

            for i in range(dist_mask.shape[1]):
                mask_T[:, i, i] = False
            
            dist_mask *= mask_T

            #初始化替换原则
            replace_index = torch.tensor(range(len(xt[0]))).repeat(B, 1).to('cuda')  #B (h w t)
            #按照连接关系排序
            sort_result, sort_result_index = torch.sum(dist_mask, dim=-1).sort() #b (h w t)

            while(sort_result.sum()!=0):    #只要还有连接关系
                max_link_index = sort_result_index[:, -1]   #b 1
                for batch_id in range(B):
                    links_index = dist_mask[batch_id][max_link_index[batch_id]].nonzero()   #squ之前 [link_num, 1]，之后link_num个数
                    repeat_token_num_time += len(links_index)
                    links_index = links_index.squeeze()
                    #更新替换关系
                    replace_index[batch_id][links_index] = max_link_index[batch_id]
                    #更新mask
                    dist_mask[batch_id, max_link_index[batch_id]] = False
                    dist_mask[batch_id, :, max_link_index[batch_id]] = False
                    dist_mask[batch_id, links_index] = False
                    dist_mask[batch_id, :, links_index] = False
                #更新排序
                sort_result, sort_result_index = torch.sum(dist_mask, dim=-1).sort() #b (h w t)
            for batch_id in range(B):
                xt[batch_id] = xt[batch_id].index_select(0, replace_index[batch_id])
            #--------------------------------------------------------------------------------------------------------------------------------#
            xt = rearrange(xt, 'b (h w t) m -> (b h w) t m',b=B,h=H,w=W,t=T)
            #-----------------------------------------------------------------
            #res_temporal, soft = self.temporal_attn(self.temporal_norm1(xt))
            #global soft_result_time
            #soft_result_time = torch.cat((soft_result_time, soft.unsqueeze(0)))
            #res_temporal = self.drop_path(res_temporal)
            #-----------------------------------------------------------------
            res_temporal = self.drop_path(self.temporal_attn(self.temporal_norm1(xt)))
            res_temporal = rearrange(res_temporal, '(b h w) t m -> b (h w t) m',b=B,h=H,w=W,t=T)
            res_temporal = self.temporal_fc(res_temporal)
            xt = x[:,1:,:] + res_temporal
            #------------------------------------------------------------------------------------------------------------------------#
            #if(_layer_mark==0):
            #    print('attn_time = ',xt.shape)
            #------------------------------------------------------------------------------------------------------------------------#
            ## Spatial

            init_cls_token = x[:,0,:].unsqueeze(1)
            cls_token = init_cls_token.repeat(1, T, 1)
            cls_token = rearrange(cls_token, 'b t m -> (b t) m',b=B,t=T).unsqueeze(1)
            xs = xt
            #--------------------------------------------------------------------------------------------------------------------------------#
            #cls位置不能替换
            #------------------------
            #更关注原始数据，在normal之前做也合理，但可能阈值要变大
            input_thres = 5.0
            #先换维度
            #xs = rearrange(xs, 'b (h w t) m -> b (t h w) m',b=B,h=H,w=W,t=T)
            #直接frame*token_num展平，全求距离
            dist_map = torch.cdist(xs, xs, p=2).to('cuda')
            dist_mask = dist_map<input_thres #b (t h w) (t h w)

            #对角线为0，自己和自己不连
            dig_mask = torch.eye(dist_mask.shape[1], dtype=torch.bool).to('cuda')
            dist_mask *= (~dig_mask)

            #只能patch内连，且自己不能连
            #mask_S = torch.zeros(dist_mask.shape, dtype=torch.bool).to('cuda')
            #for frame_id in range(T):
            #    mask_S[:, frame_id*H*W:(frame_id+1)*H*W, frame_id*H*W:(frame_id+1)*H*W] = True

            #for i in range(dist_mask.shape[1]):
            #    mask_S[:, i, i] = False

            #dist_mask *= mask_S

            #初始化替换原则
            replace_index = torch.tensor(range(len(xt[0]))).repeat(B, 1).to('cuda')  #B (h w t)
            #按照连接关系排序
            sort_result, sort_result_index = torch.sum(dist_mask, dim=-1).sort() #b (h w t)

            while(sort_result.sum()!=0):    #只要还有连接关系
                max_link_index = sort_result_index[:, -1]   #b 1
                for batch_id in range(B):
                    links_index = dist_mask[batch_id][max_link_index[batch_id]].nonzero()   #squ之前 [link_num, 1]，之后link_num个数
                    repeat_token_num_space += len(links_index)
                    links_index = links_index.squeeze()
                    #更新替换关系
                    replace_index[batch_id][links_index] = max_link_index[batch_id]
                    #更新mask
                    dist_mask[batch_id, max_link_index[batch_id]] = False
                    dist_mask[batch_id, :, max_link_index[batch_id]] = False
                    dist_mask[batch_id, links_index] = False
                    dist_mask[batch_id, :, links_index] = False
                #更新排序
                sort_result, sort_result_index = torch.sum(dist_mask, dim=-1).sort() #b (h w t)
            for batch_id in range(B):
                xs[batch_id] = xs[batch_id].index_select(0, replace_index[batch_id])
            #------------------------            
            #--------------------------------------------------------------------------------------------------------------------------------#
            xs = rearrange(xs, 'b (h w t) m -> (b t) (h w) m',b=B,h=H,w=W,t=T)
            xs = torch.cat((cls_token, xs), 1)
            
            #--------------------------------------------------------------------------------------------------------------------------------#
            #res_spatial, soft = self.attn(self.norm1(xs))
            #global soft_result_space
            #soft_result_space = torch.cat((soft_result_space, soft.unsqueeze(0)))
            #res_spatial = self.drop_path(res_spatial)
            res_spatial = self.drop_path(self.attn(self.norm1(xs)))
            #------------------------------------------------------------------------------------------------------------------------#
            #if(_layer_mark==0):
            #    print('attn_space = ',x.shape)
            #------------------------------------------------------------------------------------------------------------------------#

            ### Taking care of CLS token
            cls_token = res_spatial[:,0,:]
            cls_token = rearrange(cls_token, '(b t) m -> b t m',b=B,t=T)
            cls_token = torch.mean(cls_token,1,True) ## averaging for every frame
            res_spatial = res_spatial[:,1:,:]
            res_spatial = rearrange(res_spatial, '(b t) (h w) m -> b (h w t) m',b=B,h=H,w=W,t=T)
            res = res_spatial
            x = xt

            ## Mlp
            x = torch.cat((init_cls_token, x), 1) + torch.cat((cls_token, res), 1)
            x = x + self.drop_path(self.mlp(self.norm2(x)))
            return x

class PatchEmbed(nn.Module):
    """ Image to Patch Embedding
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, embed_dim=768):
        super().__init__()
        img_size = to_2tuple(img_size)
        patch_size = to_2tuple(patch_size)
        num_patches = (img_size[1] // patch_size[1]) * (img_size[0] // patch_size[0])
        self.img_size = img_size
        self.patch_size = patch_size
        self.num_patches = num_patches

        self.proj = nn.Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=patch_size)

    def forward(self, x):
        B, C, T, H, W = x.shape
        x = rearrange(x, 'b c t h w -> (b t) c h w')
        x = self.proj(x)
        W = x.size(-1)
        x = x.flatten(2).transpose(1, 2)
        return x, T, W


class VisionTransformer(nn.Module):
    """ Vision Transformere
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, num_classes=1000, embed_dim=768, depth=12,
                 num_heads=12, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop_rate=0., attn_drop_rate=0.,
                 drop_path_rate=0.1, hybrid_backbone=None, norm_layer=nn.LayerNorm, num_frames=8, attention_type='divided_space_time', dropout=0.):
        super().__init__()
        self.attention_type = attention_type
        self.depth = depth
        self.dropout = nn.Dropout(dropout)
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        self.patch_embed = PatchEmbed(
            img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=embed_dim)
        num_patches = self.patch_embed.num_patches

        ## Positional Embeddings
        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches+1, embed_dim))
        self.pos_drop = nn.Dropout(p=drop_rate)
        if self.attention_type != 'space_only':
            self.time_embed = nn.Parameter(torch.zeros(1, num_frames, embed_dim))
            self.time_drop = nn.Dropout(p=drop_rate)

        ## Attention Blocks
        dpr = [x.item() for x in torch.linspace(0, drop_path_rate, self.depth)]  # stochastic depth decay rule
        self.blocks = nn.ModuleList([
            Block(
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=dpr[i], norm_layer=norm_layer, attention_type=self.attention_type)
            for i in range(self.depth)])
        self.norm = norm_layer(embed_dim)

        # Classifier head
        self.head = nn.Linear(embed_dim, num_classes) if num_classes > 0 else nn.Identity()

        trunc_normal_(self.pos_embed, std=.02)
        trunc_normal_(self.cls_token, std=.02)
        self.apply(self._init_weights)

        ## initialization of temporal attention weights
        if self.attention_type == 'divided_space_time':
            i = 0
            for m in self.blocks.modules():
                m_str = str(m)
                if 'Block' in m_str:
                    if i > 0:
                      nn.init.constant_(m.temporal_fc.weight, 0)
                      nn.init.constant_(m.temporal_fc.bias, 0)
                    i += 1

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)

    @torch.jit.ignore
    def no_weight_decay(self):
        return {'pos_embed', 'cls_token', 'time_embed'}

    def get_classifier(self):
        return self.head

    def reset_classifier(self, num_classes, global_pool=''):
        self.num_classes = num_classes
        self.head = nn.Linear(self.embed_dim, num_classes) if num_classes > 0 else nn.Identity()

    def forward_features(self, x):
        B = x.shape[0]
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('input = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        x, T, W = self.patch_embed(x)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('patch_embed = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        cls_tokens = self.cls_token.expand(x.size(0), -1, -1)
        x = torch.cat((cls_tokens, x), dim=1)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('patch_embed_cls = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#

        ## resizing the positional embeddings in case they don't match the input at inference
        if x.size(1) != self.pos_embed.size(1):
            pos_embed = self.pos_embed
            cls_pos_embed = pos_embed[0,0,:].unsqueeze(0).unsqueeze(1)
            other_pos_embed = pos_embed[0,1:,:].unsqueeze(0).transpose(1, 2)
            P = int(other_pos_embed.size(2) ** 0.5)
            H = x.size(1) // W
            other_pos_embed = other_pos_embed.reshape(1, x.size(2), P, P)
            new_pos_embed = F.interpolate(other_pos_embed, size=(H, W), mode='nearest')
            new_pos_embed = new_pos_embed.flatten(2)
            new_pos_embed = new_pos_embed.transpose(1, 2)
            new_pos_embed = torch.cat((cls_pos_embed, new_pos_embed), 1)
            x = x + new_pos_embed
        else:
            x = x + self.pos_embed
        x = self.pos_drop(x)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('pose_embed = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#

        ## Time Embeddings
        if self.attention_type != 'space_only':
            cls_tokens = x[:B, 0, :].unsqueeze(1)
            x = x[:,1:]
            x = rearrange(x, '(b t) n m -> (b n) t m',b=B,t=T)
            ## Resizing time embeddings in case they don't match
            if T != self.time_embed.size(1):
                time_embed = self.time_embed.transpose(1, 2)
                new_time_embed = F.interpolate(time_embed, size=(T), mode='nearest')
                new_time_embed = new_time_embed.transpose(1, 2)
                x = x + new_time_embed
            else:
                x = x + self.time_embed
            x = self.time_drop(x)
            x = rearrange(x, '(b n) t m -> b (n t) m',b=B,t=T)
            x = torch.cat((cls_tokens, x), dim=1)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('time_embed = ',x.shape)
        #global _layer_mark
        #_layer_mark = 0
        #----------------------------------------------------------------------------------------------------------------------------#

        global global_layer
        global_layer = 0 
        ## Attention blocks
        for blk in self.blocks:
            x = blk(x, B, T, W)
            global_layer += 1
            #------------------------------------------------------------------------------------------------------------------------#
            #if(_layer_mark==0):
            #    print('attn_one = ',x.shape)
            #_layer_mark += 1
            #------------------------------------------------------------------------------------------------------------------------#
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('attn_all = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#

        ### Predictions for space-only baseline
        if self.attention_type == 'space_only':
            x = rearrange(x, '(b t) n m -> b t n m',b=B,t=T)
            x = torch.mean(x, 1) # averaging predictions for every frame

        x = self.norm(x)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('timesformer = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        return x[:, 0]

    def forward(self, x):
        x = self.forward_features(x)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('head_before = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        x = self.head(x)
        #----------------------------------------------------------------------------------------------------------------------------#
        #print('head = ',x.shape)
        #----------------------------------------------------------------------------------------------------------------------------#
        return x

def _conv_filter(state_dict, patch_size=16):
    """ convert patch embedding weight from manual patchify + linear proj to conv"""
    out_dict = {}
    for k, v in state_dict.items():
        if 'patch_embed.proj.weight' in k:
            if v.shape[-1] != patch_size:
                patch_size = v.shape[-1]
            v = v.reshape((v.shape[0], 3, patch_size, patch_size))
        out_dict[k] = v
    return out_dict

@MODEL_REGISTRY.register()
class vit_base_patch16_224(nn.Module):
    def __init__(self, cfg, **kwargs):
        super(vit_base_patch16_224, self).__init__()
        self.pretrained=True
        patch_size = 16
        self.model = VisionTransformer(img_size=cfg.DATA.TRAIN_CROP_SIZE, num_classes=cfg.MODEL.NUM_CLASSES, patch_size=patch_size, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), drop_rate=0., attn_drop_rate=0., drop_path_rate=0.1, num_frames=cfg.DATA.NUM_FRAMES, attention_type=cfg.TIMESFORMER.ATTENTION_TYPE, **kwargs)

        self.attention_type = cfg.TIMESFORMER.ATTENTION_TYPE
        self.model.default_cfg = default_cfgs['vit_base_patch16_224']
        self.num_patches = (cfg.DATA.TRAIN_CROP_SIZE // patch_size) * (cfg.DATA.TRAIN_CROP_SIZE // patch_size)
        pretrained_model=cfg.TIMESFORMER.PRETRAINED_MODEL
        if self.pretrained:
            load_pretrained(self.model, num_classes=self.model.num_classes, in_chans=kwargs.get('in_chans', 3), filter_fn=_conv_filter, img_size=cfg.DATA.TRAIN_CROP_SIZE, num_patches=self.num_patches, attention_type=self.attention_type, pretrained_model=pretrained_model)

    def forward(self, x):
        x = self.model(x)
        return x

@MODEL_REGISTRY.register()
class TimeSformer(nn.Module):
    def __init__(self, img_size=224, patch_size=16, num_classes=400, num_frames=8, attention_type='divided_space_time',  pretrained_model='', **kwargs):
        super(TimeSformer, self).__init__()
        self.pretrained=True
        self.model = VisionTransformer(img_size=img_size, num_classes=num_classes, patch_size=patch_size, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4, qkv_bias=True, norm_layer=partial(nn.LayerNorm, eps=1e-6), drop_rate=0., attn_drop_rate=0., drop_path_rate=0.1, num_frames=num_frames, attention_type=attention_type, **kwargs)

        self.attention_type = attention_type
        self.model.default_cfg = default_cfgs['vit_base_patch'+str(patch_size)+'_224']
        self.num_patches = (img_size // patch_size) * (img_size // patch_size)
        if self.pretrained:
            load_pretrained(self.model, num_classes=self.model.num_classes, in_chans=kwargs.get('in_chans', 3), filter_fn=_conv_filter, img_size=img_size, num_frames=num_frames, num_patches=self.num_patches, attention_type=self.attention_type, pretrained_model=pretrained_model)
    def forward(self, x):
        x = self.model(x)
        return x
